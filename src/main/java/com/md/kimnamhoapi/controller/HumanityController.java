package com.md.kimnamhoapi.controller;

import com.md.kimnamhoapi.entity.Friend;
import com.md.kimnamhoapi.model.humanity.HumanityCreateRequest;
import com.md.kimnamhoapi.model.humanity.HumanityItem;
import com.md.kimnamhoapi.model.humanity.HumanityReceiveChangeRequest;
import com.md.kimnamhoapi.model.humanity.HumanityResponse;
import com.md.kimnamhoapi.service.FriendService;
import com.md.kimnamhoapi.service.HumanityService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/humanity")
public class HumanityController {
    private final FriendService friendService;
    private final HumanityService humanityService;

    @PostMapping("/new/friend-id/{friendId}")
    public String setHumanity(@PathVariable long friendId, @RequestBody HumanityCreateRequest request){
        Friend friend = friendService.getData(friendId);
        humanityService.setHumanity(friend, request);

        return "ok";
    }

    @GetMapping("/humanity-all")
    public List<HumanityItem> getHumanities(){
        return humanityService.getHumanities();
    }

    @GetMapping("/humanity/{id}")
    public HumanityResponse getHumanity(@PathVariable long id){
        return humanityService.getHumanity(id);
    }

    @PutMapping("/humanity-receive/{id}")
    public String putHumanityReceive(@PathVariable long id, @RequestBody HumanityReceiveChangeRequest request){
        humanityService.putHumanityReceive(id, request);

        return "ok";
    }

    @DeleteMapping("/delete/{humanityId}")
    public String delHumanity(@PathVariable long humanityId) {
        humanityService.delHumanity(humanityId);

        return "ok";
    }
}
