package com.md.kimnamhoapi.controller;

import com.md.kimnamhoapi.model.friend.*;
import com.md.kimnamhoapi.service.FriendService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/friend")
public class FriendController {
    private final FriendService friendService;

    @PostMapping("/new")
    public String setFriend(@RequestBody FriendCreateRequest request){
        friendService.setFriend(request);

        return "ok";
    }

    @GetMapping("/friend-all")
    public List<FriendItem> getFriends(){
        return friendService.getFriends();
    }

    @GetMapping("/friend/{id}")
    public FriendResponse getFriend(@PathVariable long id){
        return friendService.getFriend(id);
    }

    @PutMapping("/friend-cut-of/{id}")
    public String putFriendIsCutOff(@PathVariable long id, @RequestBody FriendIsCutOffChangeRequest request){
        friendService.putFriendIsCutOff(id, request);

        return "ok";
    }

    @PutMapping("/friend-cut-update/{cutId}")
    public String putFriendCut(@PathVariable long cutId, @RequestBody FriendCutUpdateRequest request){
        friendService.putFriendCut(cutId, request);

        return "ok";
    }

    @PutMapping("/friend-cut-initialization/{initializationId}")
    public String putFriendCutInitialization(@PathVariable long initializationId){
        friendService.putFriendCutInitialization(initializationId);

        return "ok";
    }
}
