package com.md.kimnamhoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendCreateRequest {
    private String name;
    private LocalDate birthDate;
    private String phoneNumber;
    private String etcMemo;

}
