package com.md.kimnamhoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendIsCutOffChangeRequest {
    private Boolean isCutOff;
}
