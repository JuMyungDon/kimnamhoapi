package com.md.kimnamhoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendCutInitializationRequest {
    private Boolean isCutOff;
}
