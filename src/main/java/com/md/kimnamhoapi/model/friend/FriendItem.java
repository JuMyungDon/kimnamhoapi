package com.md.kimnamhoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class FriendItem {
    private String name;
    private LocalDate birthDate;
    private String phoneNumber;
    private String etcMemo;
    private String isCutOff;
    private LocalDate cutDate;
    private String reason;
}
