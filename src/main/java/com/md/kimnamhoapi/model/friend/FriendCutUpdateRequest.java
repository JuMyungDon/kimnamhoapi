package com.md.kimnamhoapi.model.friend;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FriendCutUpdateRequest {
    private String reason;
}
