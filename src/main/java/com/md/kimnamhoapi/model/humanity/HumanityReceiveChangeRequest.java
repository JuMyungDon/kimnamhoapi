package com.md.kimnamhoapi.model.humanity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HumanityReceiveChangeRequest {
    private String category;
    private Double price;
}
