package com.md.kimnamhoapi.model.humanity;

import com.md.kimnamhoapi.entity.Friend;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityItem {
    private Long friendId;
    private String type;
    private String category;
    private Double price;
    private LocalDate giftDate;
}
