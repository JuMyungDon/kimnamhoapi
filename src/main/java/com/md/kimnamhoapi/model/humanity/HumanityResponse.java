package com.md.kimnamhoapi.model.humanity;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class HumanityResponse {
    private Long friendId;
    private String type;
    private String category;
    private Double price;
    private LocalDate giftDate;
}
