package com.md.kimnamhoapi.service;

import com.md.kimnamhoapi.entity.Friend;
import com.md.kimnamhoapi.model.friend.*;
import com.md.kimnamhoapi.repository.FriendRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FriendService {
    private final FriendRepository friendRepository;

    public Friend getData(long id){
        return friendRepository.findById(id).orElseThrow();
    }

    public void setFriend(FriendCreateRequest request){
        Friend addData = new Friend();
        addData.setName(request.getName());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());
        addData.setIsCutOff(false);

        friendRepository.save(addData);
    }

    public List<FriendItem> getFriends(){
        List<Friend> originList = friendRepository.findAll();

        List<FriendItem> result = new LinkedList<>();

        for (Friend friend : originList){
            FriendItem addItem = new FriendItem();
            addItem.setName(friend.getName());
            addItem.setBirthDate(friend.getBirthDate());
            addItem.setPhoneNumber(friend.getPhoneNumber());
            addItem.setEtcMemo(friend.getEtcMemo());
            addItem.setIsCutOff(friend.getIsCutOff() ? "예" : "아니오");
            addItem.setCutDate(friend.getCutDate());
            addItem.setReason(friend.getReason());

            result.add(addItem);
        }

        return result;
    }

    public FriendResponse getFriend(long friendId){
        Friend originData = friendRepository.findById(friendId).orElseThrow();

        FriendResponse response = new FriendResponse();
        response.setName(originData.getName());
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());
        response.setIsCutOff(originData.getIsCutOff() ? "예" : "아니요");
        response.setCutDate(originData.getCutDate());
        response.setReason(originData.getReason());

        return response;
    }

    public void putFriendIsCutOff(long id, FriendIsCutOffChangeRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCutOff(request.getIsCutOff());

        friendRepository.save(originData);
    }

    public void putFriendCut(long id, FriendCutUpdateRequest request){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCutOff(true);
        originData.setCutDate(LocalDate.now());
        originData.setReason(request.getReason());

        friendRepository.save(originData);
    }

    public void putFriendCutInitialization(long id){
        Friend originData = friendRepository.findById(id).orElseThrow();
        originData.setIsCutOff(false);
        originData.setCutDate(null);
        originData.setReason(null);

        friendRepository.save(originData);
    }
}
