package com.md.kimnamhoapi.service;

import com.md.kimnamhoapi.entity.Friend;
import com.md.kimnamhoapi.entity.Humanity;
import com.md.kimnamhoapi.model.humanity.HumanityCreateRequest;
import com.md.kimnamhoapi.model.humanity.HumanityItem;
import com.md.kimnamhoapi.model.humanity.HumanityReceiveChangeRequest;
import com.md.kimnamhoapi.model.humanity.HumanityResponse;
import com.md.kimnamhoapi.repository.HumanityRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;


@Service
@RequiredArgsConstructor
public class HumanityService {
    private final HumanityRepository humanityRepository;

    public void setHumanity(Friend friend, HumanityCreateRequest request){
        Humanity addData = new Humanity();
        addData.setFriend(friend);
        addData.setType(request.getType());
        addData.setCategory(request.getCategory());
        addData.setPrice(request.getPrice());
        addData.setGiftDate(request.getGiftDate());

        humanityRepository.save(addData);
    }

    public List<HumanityItem> getHumanities(){
        List<Humanity> originList = humanityRepository.findAll();

        List<HumanityItem> result = new LinkedList<>();

        for (Humanity humanity : originList){
            HumanityItem addItem = new HumanityItem();
            addItem.setFriendId(humanity.getFriend().getId());
            addItem.setType(humanity.getType() ? "줌" : "받음");
            addItem.setCategory(humanity.getCategory());
            addItem.setPrice(humanity.getPrice());
            addItem.setGiftDate(humanity.getGiftDate());

            result.add(addItem);
        }

        return result;
    }

    public HumanityResponse getHumanity(long humanityId){
        Humanity originData = humanityRepository.findById(humanityId).orElseThrow();

        HumanityResponse response = new HumanityResponse();
        response.setFriendId(originData.getFriend().getId());
        response.setType(originData.getType() ? "줌" : "받음");
        response.setCategory(originData.getCategory());
        response.setPrice(originData.getPrice());
        response.setGiftDate(originData.getGiftDate());

        return response;
    }

    public void putHumanityReceive(long id, HumanityReceiveChangeRequest request){
        Humanity originData = humanityRepository.findById(id).orElseThrow();
        originData.setCategory(originData.getCategory());
        originData.setPrice(originData.getPrice());

        humanityRepository.save(originData);
    }

    public void delHumanity(long id){
        humanityRepository.deleteById(id);
    }
}
