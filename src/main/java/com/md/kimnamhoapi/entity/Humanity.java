package com.md.kimnamhoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Humanity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "friendId", nullable = false)
    private Friend friend;

    @Column(nullable = false)
    private Boolean type;

    @Column(nullable = false, length = 30)
    private String category;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate giftDate;
}