package com.md.kimnamhoapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Friend {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 15, unique = true)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;

    @Column(nullable = false)
    private Boolean isCutOff;

    private LocalDate cutDate;

    @Column(length = 30)
    private String reason;
}
