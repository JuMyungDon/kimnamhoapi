package com.md.kimnamhoapi.repository;

import com.md.kimnamhoapi.entity.Humanity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumanityRepository extends JpaRepository<Humanity, Long> {
}
