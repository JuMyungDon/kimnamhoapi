package com.md.kimnamhoapi.repository;

import com.md.kimnamhoapi.entity.Friend;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FriendRepository extends JpaRepository<Friend, Long> {
}
