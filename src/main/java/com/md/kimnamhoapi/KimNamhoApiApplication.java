package com.md.kimnamhoapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KimNamhoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(KimNamhoApiApplication.class, args);
    }

}
